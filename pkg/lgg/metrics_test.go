package lgg

import (
	"errors"
	"testing"
	"time"
)

func TestGetMetrics(t *testing.T) {
	provider := NewHttpMetricsProvider("client.latency.gg", "true", 15*time.Second)
	metrics, err := provider.Get("aws", "eu-central-1", "35.68.21.42")
	if err != nil {
		t.Error(err)
	}

	if len(metrics.Beacons) == 0 {
		t.Error(errors.New("expected list of providers"))
	}
}
