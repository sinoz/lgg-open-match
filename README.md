# LGG Open-Match

An easy to use library to integrate Open-Match and Latency.GG.

## Getting Started

Instantiate a new instance of:

```go
l := lgg.NewLGG(lgg.Credentials{
	BaseUrl: "https://client.latency.gg",
	ApiKey:  "true",
	Timeout: 5 * time.Second,
})
```

Given the following `makeMatches` function that partitions tickets-and generates `Match` instances (taken from the Open-Match tutorial):

```go
func makeMatches(
	p *pb.MatchProfile,
	poolTickets map[string][]*pb.Ticket,
) []*pb.Match {
	var matches []*pb.Match
	count := 0
	for {
		insufficientTickets := false
		matchTickets := []*pb.Ticket{}
		for pool, tickets := range poolTickets {
			if len(tickets) < ticketsPerPoolPerMatch {
				// This pool is completely drained out. Stop creating matches.
				insufficientTickets = true
				break
			}

			// Remove the Tickets from this pool and add to the match proposal.
			matchTickets = append(matchTickets, tickets[0:ticketsPerPoolPerMatch]...)
			poolTickets[pool] = tickets[ticketsPerPoolPerMatch:]
		}

		if insufficientTickets {
			break
		}

		matches = append(matches, &pb.Match{
			MatchId:       fmt.Sprintf("profile-%v-time-%v-%v", p.GetName(), time.Now().Format("2006-01-02T15:04:05.00"), count),
			MatchProfile:  p.GetName(),
			MatchFunction: matchName,
			Tickets:       matchTickets,
		})

		count++
	}

	return matches
}
```

We then call `makeMatches` inside our MatchFunction and use our new instantiated `LGG` instance to call `SearchBestLatencies` with:

```go
func (s *MatchFunctionService) Run(req *pb.RunRequest, stream pb.MatchFunction_RunServer) error {
	matches := makeMatches(req.Profile, poolOfTickets)
	if s.lgg != nil {
		errors := s.lgg.SearchBestLatencies(defaultProvider, matches)
		for _, err := range errors {
			// TODO do something with `err` here!
		}
	}
	
	// ... here we stream the results back to Open-Match
}
```

The `SearchBestLatencies` function will call LatencyGG for every ticket for a set of locations against an IP address supplied by the ticket. The results for each ticket will then also be stored into the ticket. Each ticket must have an `ipAddress` and a `locations` extension for the search function to search the best location/latency pair for.

For example, when creating a ticket:

```go
	extensions := make(map[string]*any.Any)

	ipAddr, _ := ptypes.MarshalAny(&proto.IPAddress{ Value: "IP_ADDRESS_HERE" })
	locs, _ := ptypes.MarshalAny(&proto.LocationSet{ Locations: []string{"eu-central-1"} })

	extensions["ipAddress"] = ipAddr
	extensions["locations"] = locs

	ticket := &pb.Ticket{
		Extensions: extensions,
	}
```

The results are stored under the `lggValues` key and can be extracted as Protobuf extensions. The type of the associated value is `proto.LatencySet`, which holds a list of location/latency pairs.